const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports.default = {
  name: "base",
  entry: [
    "./src/index.tsx",
    "./src/styles/index.scss"
  ],
  output: {
    filename: "bundle.js",
    path: path.join(__dirname, "../dist")
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Developer Panel",
      filename: "index.html",
      inject: false,
      template: path.join(__dirname, "index.html")
    }),
    new webpack.DefinePlugin({
      "process.env.API_HOST": JSON.stringify("http://localhost:4500/api")
    })
  ],

  devServer: {
    index: "index.html",
    contentBase: path.join(__dirname, "../dist"),
    compress: true,
    port: 3001,
    host: "0.0.0.0",
    historyApiFallback: true,
    publicPath: "/"
  },

  mode: "development",
  devtool: "source-map",

  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all"
        }
      }
    }
  },

  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: [".ts", ".tsx", ".js", ".json"]
  },

  module: {
    rules: [
      // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
      {
        test: /\.tsx?$/,
        loader: "awesome-typescript-loader"
      },

      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader"
      },
      {
        test: /\.scss$/,
        use: [{
            loader: "file-loader",
            options: {
              name: "../dist/css/main.css"
            }
          },
          {
            loader: "extract-loader"
          },
          {
            loader: "css-loader"
          },
          {
            loader: "sass-loader"
          }
        ]
      }
    ]
  }
}