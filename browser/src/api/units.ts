import axios from "axios";

export const client = axios.create({
  baseURL: `${process.env.API_HOST}/`,
  timeout: 10000
});

export const getUnits = () => {
  return client.get(`units`);
};
