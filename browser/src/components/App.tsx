import * as React from "react";
import { getUnits } from "../api/units";

interface IAppState {
  units: any[];
}

export default class App extends React.Component<{}, IAppState> {
  state = {
    units: []
  };

  async componentDidMount() {
    const result = await getUnits();
    this.setState({ units: result.data });
  }

  render() {
    return (
      <div className="container">
        <h2>Course</h2>
        <strong>Unit List:</strong>
        <ul>
          {this.state.units.map((unit, key) => (
            <li key={key}>{unit.name}</li>
          ))}
        </ul>
      </div>
    );
  }
}
