# Bondi Labs Full-Stack Code Test

Welcome to the Bondi Labs full-stack code test. This code test has been designed to test your knowledge of React, Express, HTML/CSS, and how websites interact with severs in a 'vertical slice'.

Your task is to finish a small application which manages a list of Units in a Course. You should be able to add and delete Units from this course and have the results refreshed on the browser, using the partially-completed template we have provided. The frontend and backend code should be written well and easily readable.

Please spend no longer than **1-2 hours** on this task.

A fully completed solution might look a little like [this](https://gyazo.com/1f5d71f92631fe1fce820b7dab9f8c07).

## Goals

You should aim to complete as many of these goals within the timeframe.

- Allow the user to add new Units to the course (frontend + backend work)
- Allow the user to delete Units from the course (frontend + backend work)
- Refactor the Units into their own React components in the frontend
- Show the suggested completion time for each unit
- Show the course information in the frontend

## Stretch Goals

If you complete the above and still have time remaining, these are some extra bonus point tasks!

- Add some better styling to the frontend
- Annotate the backend and frontend with Typescript types for the Units and Course Information
- Allow the user to create new courses and switch between those courses

## Getting Started

1. Run `npm install` in both the `browser` and `server` folders.
2. Start the server with `npm start` in the `server` folder - it will be available on `localhost:4500` by default.
3. Start the browser with `npm start` in the `browser` folder. It will be available on `localhost:3001` by default.
4. Use Git to track your work.
5. When you're done, email us with either a public-facing repo, pull-request, or a zipped folder of your work.

**Technologies Used:**

Here are the technologies we're using for this code test.

- ExpressJS - Server for creating, retrieving, updating, deleting routes, and handling backend data
- ReactJS - Frontend component framework
- Bootstrap - Frontend styling library
- Typescript - Static-typed superset of Javascript
- Webpack - Bundles frontend code, and converts to browser-compatible Javascript
- AxiosJS - Promise-based HTTP client for communicating with server

**Learning Resources:**

Don't worry if you're not completely familiar with some of these technologies. Learning through doing is the best way to adopt new skills, so we've provided some basic links below if you're having trouble:

- ReactJS: [https://reactjs.org/docs/hello-world.html](https://reactjs.org/docs/hello-world.html)
- ExpressJS: [https://flaviocopes.com/express/#hello-world](https://flaviocopes.com/express/#hello-world)
- Typescript: [https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)
- Bootstrap: [https://getbootstrap.com/docs/4.3/getting-started/introduction/](https://getbootstrap.com/docs/4.3/getting-started/introduction/)
