import * as express from "express";
import { getAllUnits } from "./services/UnitService";

const router = express.Router();

router.get("/units", (req: express.Request, res: express.Response) => {
  return res.json(getAllUnits());
});

router.delete("/unit/:id", (req: express.Request, res: express.Response) => {
  const unitId = req.params.id;
});

export default router;
