import * as bodyParser from "body-parser";
import * as express from "express";
import router from "./routes";
const config = require("config");

const app = express();
configureMiddleware(app);

app.use("/api", router);

app.listen(config.server.port, config.server.host, (err: any) => {
  if (err) {
    console.error("Could not create the server.");
    process.exit(-1);
  }
  console.log(`App is running at ${config.server.host}:${config.server.port}`);
});

function configureMiddleware(app) {
  app.use(bodyParser.json());

  // ## CORS middleware
  //
  // see: http://stackoverflow.com/questions/7067966/how-to-allow-cors-in-express-nodejs
  const allowCrossDomain = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");

    if (req.method === "OPTIONS") {
      res.send(200);
    } else {
      next();
    }
  };
  app.use(allowCrossDomain);
  app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send(err.message || "Something broke!");
  });
}
