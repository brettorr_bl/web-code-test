export function getCourseInformation() {
  return {
    name: "Rocketing to Mars!",
    description:
      "Learn how to build your own spaceship and escape the Earth! Easily assembled in less than thirty years."
  };
}
