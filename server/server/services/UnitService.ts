const UnitDatabase = [
  {
    id: 1,
    name: "Intro to Spaceship Design",
    time: "3 hours"
  },
  {
    id: 2,
    name: "Piloting a Spaceship: 101",
    time: "5 hours"
  }
];

export function getAllUnits() {
  return UnitDatabase;
}
